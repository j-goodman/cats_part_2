class User < ActiveRecord::Base
  validates :user_name, :password_digest, :session_token, presence: true
  validates :session_token, :user_name, uniqueness: true
  validates :password, length: { minimum: 6, allow_nil: true }

  before_validation :ensure_session_token

  has_many(
    :cats,
    class_name: "Cat",
    primary_key: :id,
    foreign_key: :user_id
  )

  attr_reader :password

  def self.find_by_credentials(user_params)
    name = user_params["user_name"]
    password = user_params["password"]
    user = User.find_by(user_name: name)
    if user.password_digest.is_password?(password)
      return user
    else
      return nil
    end
  end

  def reset_session_token!
    self.session_token = SecureRandom.urlsafe_base64
    self.save!
  end

  def ensure_session_token
    self.reset_session_token! if self.session_token.nil?
  end

  def password=(password)
    @password = password
    self.password_digest = BCrypt::Password.create(password)
  end

  def password_digest
    BCrypt::Password.new(super)
  end

end
