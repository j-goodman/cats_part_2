class SessionsController < ApplicationController

  before_action :require_signed_out, only: [ :new, :create ]

  def new
    render :new
  end

  def create
    user = User.find_by_credentials(user_params)
    if user
      login!(user)
      redirect_to cats_url
    else
      render :new
    end
  end

  def destroy
    current_user.reset_session_token! if current_user
    session[:session_token] = nil
    redirect_to cats_url
  end

  protected

  def require_signed_out
    redirect_to cats_url if current_user
  end

end
