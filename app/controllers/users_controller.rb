class UsersController < ApplicationController

  before_action :require_signed_out, only: [ :new, :create ]

  def new
    render :new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      login!(@user)
      redirect_to cats_url
    else
      render :new
    end
  end

  private

  def require_signed_out
    redirect_to cats_url if current_user
  end

end
