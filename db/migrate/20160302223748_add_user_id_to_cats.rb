class AddUserIdToCats < ActiveRecord::Migration
  def change
    add_column(:cats, :user_id, :integer)
    Cat.all.each_with_index do |cat, i|
      cat.user_id = i + 1
      cat.save!
    end
  end
end
